<?php
	
	// Add RSS links to <head> section
	automatic_feed_links();
	
	// Load jQuery
	if ( !is_admin() ) {
	   wp_deregister_script('jquery');
	   wp_register_script('jquery', ("http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"), false);
	   wp_enqueue_script('jquery');
	}
	
	// Clean up the <head>
	function removeHeadLinks() {
    	remove_action('wp_head', 'rsd_link');
    	remove_action('wp_head', 'wlwmanifest_link');
    }
    add_action('init', 'removeHeadLinks');
    remove_action('wp_head', 'wp_generator');
    
	/*
    if (function_exists('register_sidebar')) {
    	register_sidebar(array(
    		'name' => 'Sidebar Widgets',
    		'id'   => 'sidebar-widgets',
    		'description'   => 'These are widgets for the sidebar.',
    		'before_widget' => '<div id="%1$s" class="widget %2$s">',
    		'after_widget'  => '</div>',
    		'before_title'  => '<h2>',
    		'after_title'   => '</h2>'
    	));
    }
	*/
	
	/*-----------------------------------------------------------------------------------*/
	/*	Queue Scripts
	/*-----------------------------------------------------------------------------------*/
	 
	 /*
	function tk_admin_scripts() {
		wp_enqueue_script('media-upload');
		wp_enqueue_script('thickbox');
		wp_register_script('tk-upload', get_template_directory_uri() . '/functions/js/upload-button.js', array('jquery','media-upload','thickbox'));
		wp_enqueue_script('tk-upload');
	}
	function tk_admin_styles() {
		wp_enqueue_style('thickbox');
	}
	add_action('admin_print_scripts', 'tk_admin_scripts');
	add_action('admin_print_styles', 'tk_admin_styles');
	*/
	
	
	if ( function_exists( 'add_theme_support' ) ) {
		add_theme_support( 'post-thumbnails' );
		//add_image_size( 'custom-thumbnail', 800 );
	}
		
	function paginate() {  
	    global $wp_query, $wp_rewrite;  
	    $wp_query->query_vars['paged'] > 1 ? $current = $wp_query->query_vars['paged'] : $current = 1;  
	    $pagination = array(  
	        'base' => @add_query_arg('page','%#%'),  
	        'format' => '',  
	        'total' => $wp_query->max_num_pages,  
	        'current' => $current,  
	        'show_all' => true,  
	        'type' => 'plain'  
	    );  
	    if ( $wp_rewrite->using_permalinks() ) $pagination['base'] = user_trailingslashit( trailingslashit( remove_query_arg( 'post_type', get_pagenum_link( 1 ) ) ) . 'page/%#%/', 'paged' );
		/*
	    //echo $pagination['base']."<br>";
	    $pagination['base'] = strtok($pagination['base'],'?');
	    $pagination['base'] .= 'page/%#%/'; 
	    //echo $pagination['base']."<br>";
	    
	    
	    
	    
	    
	    $pagination['base'].='?'.$_SERVER['QUERY_STRING'];
	    if ( !empty($wp_query->query_vars['s']) ) $pagination['add_args'] = array( 's' => get_query_var( 's' ) );
	      */
	    /*if ($wp_query->query_vars['post_type'] && $wp_query->query_vars['post_type']!='post')
			$pagination['base'].='?post_type='.$wp_query->query_vars['post_type'];
		*/
		 
	    echo paginate_links( $pagination );
	}

	function wptuts_opengraph_for_posts() {
	    if ( is_singular() ) {
	        global $post;
	        setup_postdata( $post );
	        $output = '<meta property="og:type" content="article" />' . "\n";
	        $output .= '<meta property="og:title" content="' . esc_attr( get_the_title() ) . '" />' . "\n";
	        $output .= '<meta property="og:url" content="' . get_permalink() . '" />' . "\n";
	        $output .= '<meta property="og:description" content="' . esc_attr( get_the_excerpt() ) . '" />' . "\n";
	        if ( has_post_thumbnail() ) {
	            $imgsrc = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'endorsement-feature' );
	            $output .= '<meta property="og:image" content="' . $imgsrc[0] . '" />' . "\n";
	        }
	        echo $output;
	    }
	}
	add_action( 'wp_head', 'wptuts_opengraph_for_posts' );

/*
	//http://melchoyce.github.io/dashicons/
	function add_menu_icons_styles(){
	?>
	<style>
	
	#adminmenu .menu-icon-slide div.wp-menu-image:before {
	  content: '\f169';
	}
	
	#adminmenu .menu-icon-artist div.wp-menu-image:before {
	  content: '\f336';
	}
	#adminmenu .menu-icon-artwork div.wp-menu-image:before {
	  content: '\f128';
	}
	#adminmenu .menu-icon-exhibition div.wp-menu-image:before {
	  content: '\f161';
	}
	#adminmenu .menu-icon-press div.wp-menu-image:before {
	  content: '\f123';
	}
	</style>
	 
	<?php
	}
	add_action( 'admin_head', 'add_menu_icons_styles' );
*/

/*	menu-related
	add_action( 'init', 'register_my_menus' );
	function register_my_menus() {
		register_nav_menus(
			array(
				'header-1' => __( 'Header Menu' ),
				'resource-1' => __( 'Download Resource' ),
				'footer-1' => __( 'Footer Menu' )
			)
		);
	}
	
	function menu_fallback(){
	?>
		<ul class="sf-menu">
		  <!-- <li><a href="<?php echo get_option('home'); ?>/" class="on">Home</a></li> -->
		  <?php wp_list_pages('title_li='); ?>
		</ul>
	<?php 
	}
 
 	require_once('functions/wp_bootstrap_navwalker.php');
	
	class my_wp_bootstrap_navwalker extends wp_bootstrap_navwalker{
        function end_lvl( &$output, $depth = 0, $args = array() ) {
            $indent = str_repeat("\t", $depth);
            $output .= "$indent</ul>\n";
        }
    }

 */
 
 /* menu front-end
 	wp_nav_menu( array( 'theme_location' => 'header-1' ,'menu_class'=>'nav sf-menu','container'=>false,'fallback_cb'=>'menu_fallback') );
  
  	wp_nav_menu(array(
                    'theme_location'  => 'menu',
                    'menu'            => '',
                    'container'       => false,                    
                    'container_id'    => '',
                    'menu_class'      => 'nav navbar-nav pull-right',
                    'menu_id'         => '',
                    'echo'            => true,
                    'fallback_cb'     => 'wp_page_menu',
                    'before'          => '',
                    'after'           => '',
                    'link_before'     => '',
                    'link_after'      => '',
                    'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                    'depth'           => 0,
                    'walker'          => new wp_bootstrap_navwalker(),
                    'fallback_cb'	  =>'menu_fallback')); 
   
  	function home_page_menu_args( $args ) {
	$args['show_home'] = true;
	return $args;
	}
	add_filter( 'wp_page_menu_args', 'home_page_menu_args' );
  * /
 /*
 function qt_custom_breadcrumbs() {
  
  $showOnHome = 0; // 1 - show breadcrumbs on the homepage, 0 - don't show
  $delimiter = '&raquo;'; // delimiter between crumbs
  $home = 'Home'; // text for the 'Home' link
  $showCurrent = 1; // 1 - show current post/page title in breadcrumbs, 0 - don't show
  $before = '<span class="current">'; // tag before the current crumb
  $after = '</span>'; // tag after the current crumb
  
  global $post;
  $homeLink = get_bloginfo('url');
  
  if (is_home() || is_front_page()) {
  
    if ($showOnHome == 1) echo '<div id="crumbs"><a href="' . $homeLink . '">' . $home . '</a></div>';
  
  } else {
  
    echo '<div id="crumbs"><a href="' . $homeLink . '">' . $home . '</a> ' . $delimiter . ' ';
  
    if ( is_category() ) {
      $thisCat = get_category(get_query_var('cat'), false);
      if ($thisCat->parent != 0) echo get_category_parents($thisCat->parent, TRUE, ' ' . $delimiter . ' ');
      echo $before . 'Archive by category "' . single_cat_title('', false) . '"' . $after;
  
    } elseif ( is_search() ) {
      echo $before . 'Search results for "' . get_search_query() . '"' . $after;
  
    } elseif ( is_day() ) {
      echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
      echo '<a href="' . get_month_link(get_the_time('Y'),get_the_time('m')) . '">' . get_the_time('F') . '</a> ' . $delimiter . ' ';
      echo $before . get_the_time('d') . $after;
  
    } elseif ( is_month() ) {
      echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
      echo $before . get_the_time('F') . $after;
  
    } elseif ( is_year() ) {
      echo $before . get_the_time('Y') . $after;
  
    } elseif ( is_single() && !is_attachment() ) {
      if ( get_post_type() != 'post' ) {
        $post_type = get_post_type_object(get_post_type());
        $slug = $post_type->rewrite;
        echo '<a href="' . $homeLink . '/' . $slug['slug'] . '/">' . $post_type->labels->singular_name . '</a>';
        if ($showCurrent == 1) echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
      } else {
        $cat = get_the_category(); $cat = $cat[0];
        $cats = get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
        if ($showCurrent == 0) $cats = preg_replace("#^(.+)\s$delimiter\s$#", "$1", $cats);
        echo $cats;
        if ($showCurrent == 1) echo $before . get_the_title() . $after;
      }
  
    } elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {
      $post_type = get_post_type_object(get_post_type());
      echo $before . $post_type->labels->singular_name . $after;
  
    } elseif ( is_attachment() ) {
      $parent = get_post($post->post_parent);
      $cat = get_the_category($parent->ID); $cat = $cat[0];
      echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
      echo '<a href="' . get_permalink($parent) . '">' . $parent->post_title . '</a>';
      if ($showCurrent == 1) echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
  
    } elseif ( is_page() && !$post->post_parent ) {
      if ($showCurrent == 1) echo $before . get_the_title() . $after;
  
    } elseif ( is_page() && $post->post_parent ) {
      $parent_id  = $post->post_parent;
      $breadcrumbs = array();
      while ($parent_id) {
        $page = get_page($parent_id);
        $breadcrumbs[] = '<a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a>';
        $parent_id  = $page->post_parent;
      }
      $breadcrumbs = array_reverse($breadcrumbs);
      for ($i = 0; $i < count($breadcrumbs); $i++) {
        echo $breadcrumbs[$i];
        if ($i != count($breadcrumbs)-1) echo ' ' . $delimiter . ' ';
      }
      if ($showCurrent == 1) echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
  
    } elseif ( is_tag() ) {
      echo $before . 'Posts tagged "' . single_tag_title('', false) . '"' . $after;
  
    } elseif ( is_author() ) {
       global $author;
      $userdata = get_userdata($author);
      echo $before . 'Articles posted by ' . $userdata->display_name . $after;
  
    } elseif ( is_404() ) {
      echo $before . 'Error 404' . $after;
    }
  
    if ( get_query_var('paged') ) {
      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' (';
      echo __('Page') . ' ' . get_query_var('paged');
      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';
    }
  
    echo '</div>';
  
  }
} // end qt_custom_breadcrumbs()
  * / 
 
 /* qtranslate
	add_filter( 'wp_default_editor', create_function('', 'return "html";') );
	
	function qtranslate_edit_taxonomies(){
	   if (function_exists('qtrans_modifyTermFormFor')){	
		   $args=array(
		      'public' => true ,
		      '_builtin' => false
		   ); 
		   $output = 'object'; // or objects
		   $operator = 'and'; // 'and' or 'or'
		
		   $taxonomies = get_taxonomies($args,$output,$operator); 
		
		   if  ($taxonomies) {
		     foreach ($taxonomies  as $taxonomy ) {
		         add_action( $taxonomy->name.'_add_form', 'qtrans_modifyTermFormFor');
		         add_action( $taxonomy->name.'_edit_form', 'qtrans_modifyTermFormFor');        
		      
		     }
		   }
	   }   
	}
	add_action('admin_init', 'qtranslate_edit_taxonomies');
*/

	/* Custom Style in Editor 
	add_filter('mce_css', 'tuts_mcekit_editor_style');
	function tuts_mcekit_editor_style($url) {
	
	    if ( !empty($url) )
	        $url .= ',';
	
	    // Retrieves the plugin directory URL
	    // Change the path here if using different directories
	    $url .= get_bloginfo("template_url").'/editor-styles.css';
	
	    return $url;
	}
	
	/**
	 * Add "Styles" drop-down
	 
	add_filter( 'mce_buttons_2', 'tuts_mce_editor_buttons' );
	
	function tuts_mce_editor_buttons( $buttons ) {
	    array_unshift( $buttons, 'styleselect' );
	    return $buttons;
	}
	
	/**
	 * Add styles/classes to the "Styles" drop-down
	 
	add_filter( 'tiny_mce_before_init', 'tuts_mce_before_init' );
	
	function tuts_mce_before_init( $settings ) {
	
	    $style_formats = array(        
	        array(
	            'title' => 'Click Button (accordion - Click to Open)',
	            'selector' => 'p',
	            'classes' => 'accordion',
	        ),
	        array(
	            'title' => 'Hidden Section (accordion - Content)',
	            'block' => 'div',            
	            'classes' => 'accordion_content',
	            'wrapper'=>true
	        )
	        
	    );

    $settings['style_formats'] = json_encode( $style_formats );

    return $settings;

}
	
	//Debug Use 
	add_action('wp_head', 'show_template');
	function show_template() {
		global $template;
		print_r($template);
	}
*/ 

?>