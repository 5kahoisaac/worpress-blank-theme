<?php get_header(); ?>
<div class="content" id="content_exhibition">
	
	<?php 
	$artist_post_args = array(
	    'connected_type' => 'artist_post',
	    'connected_items' => intval(get_the_ID()),        
	    //'nopaging' => true,
	    //'orderby'=>'menu_oder',
	    //'order'=>'ASC',
	    'posts_per_page'=>-1
	    
	);
	global $meta_box_exhibition;
	$post_query = new WP_Query($artist_post_args);
	
	if($post_query->have_posts()):
	while($post_query->have_posts()): $post_query->the_post(); ;				
		
	?>
	<div class="infoWrap">
		<div class="infoLeft">
			<div class="imgBoxInfo">
                <a href="<?php the_permalink();?>">
                	<?php $thumbnail_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'post-pagination');?>
                    <img src="<?php echo $thumbnail_image_url[0];?>" width="<?php echo $thumbnail_image_url[1];?>" height="<?php echo $thumbnail_image_url[2];?>"/> 
                </a>
				<span id="pBgInfo"></span>
			</div>
		</div>
		<div class="infoRight">
			<strong class="infoTitle"><?php the_title();?></strong>
			<span class="infoTime"><?php the_time('F j, Y');?></span>
			<div class="infoContent">
			<?php the_excerpt();?>
			<a href="<?php the_permalink();?>">more</a>
			</div>
		</div>
	</div>
	<?php endwhile;endif;wp_reset_query();?>
</div>
<?php get_footer(); ?>
