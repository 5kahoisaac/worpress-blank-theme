<?php

/*-----------------------------------------------------------------------------------*/
/*	Create a new post type called artwork
/*-----------------------------------------------------------------------------------*/


function tk_create_post_type_artwork() 
{
	$labels = array(
		'name' => __( 'Artwork'),
		'singular_name' => __( 'Artwork' ),
		'rewrite' => array('slug' => __( 'Artwork' )),
		'add_new' => _x('Add New', 'Artwork'),
		'add_new_item' => __('Add New Artwork'),
		'edit_item' => __('Edit Artwork'),
		'new_item' => __('New Artwork'),
		'view_item' => __('View Artwork'),
		'search_items' => __('Search Artwork'),
		'not_found' =>  __('No Artwork found'),
		'not_found_in_trash' => __('No Artwork found in Trash'), 
		'parent_item_colon' => ''
	  );
	  
	  $args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true, 
		'query_var' => true,
		'has_archive' => true,
		'menu_icon' => '',
		'rewrite' => array( 'slug' => 'artwork' ),
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => null,
		'supports' => array('title','thumbnail')
	  ); 
	  
	  register_post_type(__( 'artwork' ),$args);
}

/*
//http://wp.tutsplus.com/tutorials/custom-post-type-pagination-chaining-method/
function artwork_posts_per_page( $query ) {  
    if ( $query->query_vars['post_type'] == 'artwork' ) 
    	$query->query_vars['posts_per_page'] = of_get_option('galleries_posts_per_page');  //of_get_option('galleries_posts_per_page') - admin-options;;  
    return $query;  
}  
if ( !is_admin() ) add_filter( 'pre_get_posts', 'artwork_posts_per_page' );  
*/




/*-----------------------------------------------------------------------------------*/
/*	All the pre-made messages for the slide post type
/*-----------------------------------------------------------------------------------*/

function tk_artwork_updated_messages( $messages ) {

  $messages[__( 'artwork' )] = 
  	array(
		0 => '', // Unused. Messages start at index 1.
		1 => sprintf( __('Artwork updated. <a href="%s">View Artwork</a>'), esc_url( get_permalink($post_ID) ) ),
		2 => __('Custom field updated.'),
		3 => __('Custom field deleted.'),
		4 => __('Artwork updated.'),
		/* translators: %s: date and time of the reartwork */
		5 => isset($_GET['reartwork']) ? sprintf( __('Artwork restored to reartwork from %s'), wp_post_reartwork_title( (int) $_GET['reartwork'], false ) ) : false,
		6 => sprintf( __('Artwork published. <a href="%s">View Artwork</a>'), esc_url( get_permalink($post_ID) ) ),
		7 => __('Artwork saved.'),
		8 => sprintf( __('Artwork submitted. <a target="_blank" href="%s">Preview Artwork</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
		9 => sprintf( __('Artwork scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Artwork</a>'),
		  // translators: Publish box date format, see http://php.net/date
		  date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
		10 => sprintf( __('Artwork draft updated. <a target="_blank" href="%s">Preview Artwork</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
  );
  
  return $messages;
  
}  

function tk_build_artwork_taxonomies(){
	register_taxonomy(__( "artwork-category" ), array(__( "artwork" )), array("hierarchical" => true, "label" => __( "Artwork Category" ), "singular_label" => __( "Category" ), "rewrite" => array('slug' => 'artwork-category', 'hierarchical' => false)));
} 

/*
add_action('restrict_manage_posts','tk_artwork_restrict_manage_posts');
function tk_artwork_restrict_manage_posts() {
	global $typenow,$wp_query;
	if ($typenow=='artwork'){
                 $args = array(
                     'show_option_all' => "Show All Categories",
                     'taxonomy'        => 'artwork-category',
                     'selected' => ( isset( $wp_query->query['artwork-category'] ) ? $wp_query->query['artwork-category'] : '' ),
                     'name'               => 'artwork-category');
				wp_dropdown_categories($args);
	}
}

add_filter( 'parse_query','tk_artwork_request' );
function tk_artwork_request( $query ) {
    $qv = &$query->query_vars;
    if ( ( $qv['artwork-category'] ) && is_numeric( $qv['artwork-category'] ) ) {
        $term = get_term_by( 'id', $qv['artwork-category'], 'artwork-category' );
        $qv['artwork-category'] = $term->slug;
    }
}
/*
add_filter( 'manage_edit-artwork_columns', 'artwork_columns' );
function artwork_columns( $columns ) {
    $columns['menu_order'] = 'Order';
    return $columns;
}

add_action( 'manage_posts_custom_column', 'populate_artwork_columns' );
function populate_artwork_columns( $column ) {
    if ( 'menu_order' == $column ) {
    	$temp_post = get_post(get_the_ID());
        echo $temp_post->menu_order;
    }
}

add_filter( 'manage_edit-artwork_sortable_columns', 'sort_artwork' );
function sort_artwork( $columns ) {
    $columns['menu_order'] = 'menu_order';
    return $columns;
}
*/


add_action( 'init', 'tk_create_post_type_artwork' );
//add_action( 'init', 'tk_build_artwork_taxonomies' );
add_filter('post_updated_messages', 'tk_artwork_updated_messages');
//add_filter('post_type_link', 'qtrans_convertURL');



?>